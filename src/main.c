#include <stdio.h>
#include "cifrado.h"

int main(){
	//Ejercicio 1
	printf("Ejercicio 1\n");
	//bytesInt(int valor)
	printf("bytesInt(1200)\n");
	bytesInt(1200);
	printf("\n");
	//bytesLong(long valor)
	printf("bytesLong(124564)\n");
	bytesLong(124564);
	printf("\n");
	//bytesFloat(float valor)
	printf("bytesFloat(12.5)\n");
	bytesFloat(12.5);
	printf("\n");
	//bytesDouble(double valor)
	printf("bytesDouble(1245.43)\n");
	bytesDouble(1245.43);
	printf("\n");

    //Ejercicio 2
	printf("Ejercicio 2, cadena=http://www.fiec.espol.edu.ec\n");
    char cadena[80]="http://www.fiec.espol.edu.ec";
    datosURL(cadena);
	printf("\n");

	//Ejercicio 3

	//direccionesArray(int array[],int num,int size)
	printf("Ejercicio 3, array[10]={1,2,3,4,4,5,6,5,7,1}, num=1, size=10\n");
	int array[10]={1,2,3,4,4,5,6,5,7,1};
	direccionesArray(array,1,10);
	printf("\n");

	//Ejercicio 4

	//imc(float peso, float altura, float *resultado)
	printf("Ejercicio 4, peso=12, altura=30\n");
	float num;
	float *resultado=&num;
	imc(12,30,resultado);
	printf("El icm es: %f\n",*resultado);
	printf("\n");

    //Ejercicios 5 
	printf("Ejercicio 5\n");
    struct producto prod;
    prod.precio=20;
    struct producto *p;
    p=&prod;
    iva(p);
    printf("El precio con iva es: %.2f\n", prod.precioIVA);
	printf("\n");
    
    //Ejercicio 6
	printf("Ejercicio 6\n");
    struct producto p1;
    struct producto p2;
    struct producto p3;
    struct producto p4;
    struct producto p5;
    struct producto *arreglo[5]={&p1,&p2,&p3,&p4,&p5};
    printf("DIreccion de p1: %p\n", &p1);
    printf("DIreccion de p2: %p\n", &p2);
    printf("DIreccion de p3: %p\n", &p3);
    printf("DIreccion de p4: %p\n", &p4);
    printf("DIreccion de p5: %p\n", &p5);
    int indices[2]={2,4};
    direcciones(arreglo, indices, 2);

    return 0;
}
