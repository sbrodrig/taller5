#include <stdio.h>
#include "cifrado.h"

void bytesGeneric(void *pointer, int size){
	unsigned char *p=pointer;
	for(int i=0;i<size;i++){
		printf("Byte %d: %x\n",i,p[i]);
	}

}

void bytesInt(int valor){
	bytesGeneric(&valor,sizeof(valor));
}

void bytesLong(long valor){
	bytesGeneric(&valor,sizeof(valor));
}

void bytesFloat(float valor){
	bytesGeneric(&valor,sizeof(valor));
}

void bytesDouble(double valor){
	bytesGeneric(&valor,sizeof(valor));
}

