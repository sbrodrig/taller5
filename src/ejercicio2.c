#include <stdio.h>
#include "cifrado.h"

#define MAX 100

int contarCaracter(char *cadena){
    int cont=0;
    while(*cadena){
        if(*cadena=='.'){
            cont++;
        }
        cadena++;
    }
    return cont;  
}

void copiarString(char *dest, char *origen, int tam){
    int i=0;    
    while(i<tam){
        *dest=*origen;
        dest++;
        origen++;
        i++;
    }
    *dest='\0';
}

void datosURL(char *url){
    int t=contarCaracter(url);
    char partes[t+1][MAX];
   
    int i=0;
    char *copia=url;
    char *origen=url; 
    while(*copia){
        if(*copia=='.'){
            copiarString(partes[i], origen, copia-origen);
            i++;
            origen=copia;
        }  
        copia++;
    } 
    copiarString(partes[i], origen, copia-origen);
    
    for(int i=0; i<5; i++){
        if(partes[0][i]==':'){
            partes[0][i]='\0';
        }
    }  

    printf("Protocolo: %s\n", partes[0]);
    if(t==4){
        printf("Dominio: %s\n", partes[2]);
        printf("Codigo Pais: %s\n", partes[4]);
        printf("Tipo: %s\n", partes[3]);
    }else{
        printf("Dominio: %s\n", partes[1]);
        printf("Codigo Pais: %s\n", partes[3]);
        printf("Tipo: %s\n", partes[2]);
    }
}

    
