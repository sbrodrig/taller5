struct producto{
    char nombre[30];
    float precio;
    char SKU[30];
    int stock;
    float precioIVA;
};

void bytesGeneric(void *pointer, int size);

void bytesInt(int valor);

void bytesLong(long valor);

void bytesFloat(float valor);

void bytesDouble(double valor);

void direccionesArray(int array[], int num,int size);

void imc(float peso, float altura, float *resultado);

int contarCaracter(char *cadena);

void copiarString(char *dest, char *origen, int tam);

void datosURL(char *url);

void iva(struct producto *prod);

void direcciones(struct producto **prod, int *arreglo, int tam);

