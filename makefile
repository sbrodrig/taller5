bin/main: obj/main.o obj/ejercicio1.o obj/ejercicio2.o obj/ejercicio3.o obj/ejercicio4.o obj/ejercicio5y6.o
	gcc -Wall -o bin/main obj/main.o obj/ejercicio1.o obj/ejercicio2.o obj/ejercicio3.o obj/ejercicio4.o obj/ejercicio5y6.o

obj/main.o: src/main.c 
	gcc -Wall -c -o obj/main.o src/main.c -I./include

obj/ejercicio1.o: src/ejercicio1.c include/cifrado.h
	gcc -Wall -c -o obj/ejercicio1.o src/ejercicio1.c -I./include

obj/ejercicio2.o: src/ejercicio2.c include/cifrado.h
	gcc -Wall -c -o obj/ejercicio2.o src/ejercicio2.c -I./include

obj/ejercicio3.o: src/ejercicio3.c include/cifrado.h
	gcc -Wall -c -o obj/ejercicio3.o src/ejercicio3.c -I./include

obj/ejercicio4.o: src/ejercicio4.c include/cifrado.h
	gcc -Wall -c -o obj/ejercicio4.o src/ejercicio4.c -I./include

obj/ejercicio5y6.o: src/ejercicio5y6.c include/cifrado.h
	gcc -Wall -c -o obj/ejercicio5y6.o src/ejercicio5y6.c -I./include

.PHONY: clean

clean:
		rm -f obj/*.o
